//
//  ViewController.swift
//  pracadomowa2
//
//  Created by Stanisław Paśkowski on 13.12.2016.
//  Copyright © 2016 paskowski. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    let colors: [String: UIColor] = [
                "Red": UIColor(red:0.91, green:0.30, blue:0.24, alpha:1.0),
                "Violet": UIColor(red:0.61, green:0.35, blue:0.71, alpha:1.0),
                "Blue": UIColor(red:0.20, green:0.60, blue:0.86, alpha:1.0),
                "Emerald": UIColor(red:0.18, green:0.80, blue:0.44, alpha:1.0)
    ]
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
    }
    
    @IBAction func buttonTouched(_ sender: ColorfulButton) {
        if sender.currentTitleColor == UIColor.white {
            sender.setTitleColor(colors[sender.title(for: .normal)!], for: .normal)
            sender.backgroundColor = UIColor.white
        } else {
            sender.setTitleColor(UIColor.white, for: .normal)
            sender.backgroundColor = colors[sender.title(for: .normal)!]
        }
    }
    
}

