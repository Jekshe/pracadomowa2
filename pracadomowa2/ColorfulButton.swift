//
//  ColorfulButton.swift
//  pracadomowa2
//
//  Created by Stanisław Paśkowski on 15.12.2016.
//  Copyright © 2016 paskowski. All rights reserved.
//

import UIKit

class ColorfulButton: UIButton {

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        clipsToBounds = true
        layer.borderColor = titleColor(for: .normal)?.cgColor
        layer.borderWidth = 2
    }
//    override func draw(_ rect: CGRect) {
//        super.draw(rect)
//        
//    }

}
